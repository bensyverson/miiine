# Cashish Data models

### SKU
- skuId
- stripeId
- name
- json

### Order: GET, POST, PATCH /order
- orderId
- stripeId
- status (match Stripe)
- date
- amount
- json

### Charge: POST /charge
- orderId
- stripeId

### Customer: GET /customer
- customerId
- stripeId

### Shipment
- shipmentId
- shippoId
- orderId
- trackingURL
- labelURL

## Associations

### OrderSKUs
- orderId
- skuId
- quantity

### CustomerOrders
- customerId
- orderId
