//
//  InvitationController.swift
//  Vapor 2 Authentication
//
//  Created by Jakob Grumsen on 26/09/2017.
//

import Vapor
import HTTP
import AuthProvider
import Crypto

///RESTful interactions with our Users table
final class InvitationController {
	
	private let routeBuilder: RouteBuilder
	
	init(routeBuilder: RouteBuilder) {
		self.routeBuilder = routeBuilder
	}
	
	func addRoutes() {
		routeBuilder.post("invitation", handler: createInvitation)
		routeBuilder.put("invitation", Int.parameter, handler: updateInvitation)
		routeBuilder.delete("invitation", Int.parameter, handler: deleteInvitation)
	}
	
	func createInvitation(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()

		guard let name = request.data["name"]?.string else {
			throw Abort(.badRequest, reason: "Please provide a name")
		}
		guard let auth = request.data["authorization"]?.string else {
			throw Abort(.badRequest, reason: "Please provide an authorization level")
		}
		guard let anAuthorization = Authorization(rawValue: auth) else {
			throw Abort(.badRequest, reason: "Please provide a valid authorization level")
		}
		guard anAuthorization.intValue <= user.authorization.intValue else {
			throw Abort(.badRequest, reason: "You can't invite someone above your own level.")
		}
		
		let key = try Token.randomToken()
		let token = try Token.randomToken()

		let invitation = try Invitation(key: key, token: token, name: name, authorization: anAuthorization)
		try invitation.save()
		return try invitation.makeJSON()
	}
	
	func updateInvitation(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		let invitationId = try request.parameters.next(Int.self)
		
		guard let invitation = try Invitation.find(invitationId) else {
			throw Abort(.badRequest, reason: "Couldn't find that invitation.")
		}
		guard let auth = request.data["authorization"]?.string else {
			throw Abort(.badRequest, reason: "Please provide an authorization level")
		}
		guard let anAuthorization = Authorization(rawValue: auth) else {
			throw Abort(.badRequest, reason: "Please provide a valid authorization level")
		}
		guard anAuthorization.intValue <= user.authorization.intValue else {
			throw Abort(.badRequest, reason: "You can't change an invitation above your own level.")
		}
		
		invitation.authorization = anAuthorization
		try invitation.save()
		return try invitation.makeJSON()
	}

	func deleteInvitation(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		let invitationId = try request.parameters.next(Int.self)
		
		guard let invitation = try Invitation.find(invitationId) else {
			throw Abort(.badRequest, reason: "Couldn't find that invitation.")
		}
		guard invitation.authorization.intValue <= user.authorization.intValue else {
			throw Abort(.badRequest, reason: "You can't delete an invitation above your level.")
		}
	
		try invitation.delete()
		return try JSON(node :["success": true])
	}
}
