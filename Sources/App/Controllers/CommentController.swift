//
//  CommentController.swift
//  Vapor 2 Authentication
//
//  Created by Jakob Grumsen on 26/09/2017.
//

import Vapor
import HTTP
import AuthProvider

///RESTful interactions with our Users table
final class CommentController {
	
	private let routeBuilder: RouteBuilder
	
	init(routeBuilder: RouteBuilder) {
		self.routeBuilder = routeBuilder
	}
	
	func addRoutes() {
		routeBuilder.get("comment", Int.parameter, handler: getComment)
		routeBuilder.put("comment", Int.parameter, handler: updateComment)
		routeBuilder.delete("comment", Int.parameter, handler: deleteComment)
		routeBuilder.post("comment", handler: createComment)
	}
	
	func createComment(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()

		guard let content = request.data["content"]?.string else {
			throw Abort(.badRequest, reason: "Please provide some content")
		}
		guard let contentKey = request.data["contentKey"]?.string else {
			throw Abort(.badRequest, reason: "Please provide an idempotent key")
		}
		
		let existingComment = try Comment.makeQuery().filter("contentKey", contentKey).filter("userId", user.id).first()
		if existingComment != nil {
			throw Abort(.badRequest, reason: "Already have a comment with this idempotent key.")
		}
		
		guard let postId = request.data["postId"]?.int else {
			throw Abort(.badRequest, reason: "Please provide a post ID")
		}
		guard let post = try Post.find(postId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let comment = try Comment(post: post, user: user, contentKey: contentKey, content: content)
		try comment.save()
		return try comment.makeJSON()
	}
	
	func getComment(request: Request) throws -> ResponseRepresentable {
		let commentId = try request.parameters.next(Int.self)
		let user = try request.user()
		guard let comment = try Comment.find(commentId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		if user.id != comment.user.parentId {
			throw Abort(.badRequest, reason: "Can only view arbitrary comments you own.")
		}
		return comment
	}
	
	func updateComment(request: Request) throws -> ResponseRepresentable {
		let commentId = try request.parameters.next(Int.self)
		
		guard let content = request.json?["content"]?.string else {
			throw Abort(.badRequest, reason: "You must provide content")
		}
		
		guard let comment = try Comment.find(commentId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let user = try request.user()
		if user.id != comment.user.parentId {
			throw Abort(.badRequest, reason: "Can only edit posts you own.")
		}
		
		comment.content = content
		
		try comment.save()
		
		return try comment.makeJSON()
	}

	func deleteComment(request: Request) throws -> ResponseRepresentable {
		let commentId = try request.parameters.next(Int.self)
		
		guard let comment = try Comment.find(commentId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let user = try request.user()
		
		if user.id != comment.user.parentId {
			throw Abort(.badRequest, reason: "Can only delete posts you own.")
		}
		
		try comment.delete()
		return try JSON(node :["success": true])
	}
}
