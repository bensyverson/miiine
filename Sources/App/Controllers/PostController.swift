//
//  PostController.swift
//  Vapor 2 Authentication
//
//  Created by Jakob Grumsen on 26/09/2017.
//

import Vapor
import HTTP
import AuthProvider
import Foundation

///RESTful interactions with our Users table
final class PostController {
	
	private let routeBuilder: RouteBuilder
	
	init(routeBuilder: RouteBuilder) {
		self.routeBuilder = routeBuilder
	}
	
	func addRoutes() {
		routeBuilder.get("post", handler: getAllPosts)
		routeBuilder.get("post", Int.parameter, handler: getPost)
		routeBuilder.put("post", Int.parameter, handler: updatePost)
		routeBuilder.delete("post", Int.parameter, handler: deletePost)
		routeBuilder.post("post", "like", handler: likePost)
		routeBuilder.delete("post", "like", handler: deleteLike)
		routeBuilder.post("post", handler: createPost)
	}
	
	func urlForFilename(filename: String) throws -> URL {
		let config = try Config()
		guard let imagePath = config["app", "imagePath"]?.string else {
			throw Abort(.badRequest, reason: "Error: you need to set imagePath in the /Config/app.json file.")
		}
		let startIndex = filename.index(filename.startIndex, offsetBy: 1)
		let nextIndex = filename.index(filename.startIndex, offsetBy: 2)
		let firstChar = filename[..<startIndex]
		let firstTwoChars = filename[..<nextIndex]
		return URL(fileURLWithPath: imagePath)
			.appendingPathComponent(String(firstChar), isDirectory: true)
			.appendingPathComponent(String(firstTwoChars), isDirectory: true)
			.appendingPathComponent(filename)
	}
	
	func createPost(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		guard let bytes = request.data["image"]?.bytes else {
			throw Abort(.badRequest, reason: "Please provide an image")
		}
		let data = Data.init(bytes: bytes)
		let filename = try Token.randomToken()
		let fileWithExtension = "\(filename).jpg"
		let url = try urlForFilename(filename: fileWithExtension)
		let dir = url.deletingLastPathComponent()
		try FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
		try Data(data).write(to: url)
		
		guard let caption = request.data[Post.Keys.caption.rawValue]?.string else {
			throw Abort(.badRequest, reason: "Please provide a caption, even if it's blank.")
		}
		
		let location : BasicLocation?
		if let lat = request.data[Post.Keys.latitude.rawValue]?.string?.double,
			let lon = request.data[Post.Keys.longitude.rawValue]?.string?.double,
			let locName = request.data[Post.Keys.locationName.rawValue]?.string {
			location = BasicLocation(latitude: lat, longitude: lon, name: locName)
		} else {
			location = nil
		}
		let post = try Post(caption: caption, user: user, filename: url.absoluteString, mimeType: "image/jpeg", location: location)
		try post.save()
		return try post.makeJSON()
	}
	
	func likePost(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		guard let postId = request.data["postId"]?.int else {
			throw Abort(.badRequest, reason: "Please provide a post ID")
		}

		guard let post = try Post.find(postId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let existingLike = try Like.makeQuery().filter("postId", postId).filter("userId", user.id).first()
		if existingLike != nil {
			throw Abort(.badRequest, reason: "Already liked this comment.")
		}
		
		let like = try Like(post: post, user: user)
		try like.save()
		return try JSON(node :["success": true])
	}
	
	func getPost(request: Request) throws -> ResponseRepresentable {
		let postId = try request.parameters.next(Int.self)
		let user = try request.user()
		guard let post = try Post.find(postId) else {
			throw Abort(.badRequest, reason: "Could'nt find any post with that ID")
		}
		
		if user.id != post.user.parentId {
			throw Abort(.badRequest, reason: "Can only view posts you own.")
		}
		return post
	}
	
	func getAllPosts(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		return try user.posts.all().makeJSON()
	}
	
	func updatePost(request: Request) throws -> ResponseRepresentable {
		let postId = try request.parameters.next(Int.self)
		
		guard let caption = request.json?["caption"]?.string else {
			throw Abort(.badRequest, reason: "You must provide caption")
		}
		
		guard let post = try Post.find(postId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let user = try request.user()
		if user.id != post.user.parentId {
			throw Abort(.badRequest, reason: "Can only edit posts you own.")
		}
		
		post.caption = caption
		
		try post.save()
		
		return try post.makeJSON()
	}

	func deletePost(request: Request) throws -> ResponseRepresentable {
		let postId = try request.parameters.next(Int.self)
		
		guard let post = try Post.find(postId) else {
			throw Abort(.badRequest, reason: "Couldn't find any post with that ID")
		}
		
		let user = try request.user()
		
		if user.id != post.user.parentId {
			throw Abort(.badRequest, reason: "Can only delete posts you own.")
		}
		
		try post.delete()		
		return try JSON(node :["success": true])
	}
	
	func deleteLike(request: Request) throws -> ResponseRepresentable {
		let user = try request.user()
		guard let postId = request.data["postId"]?.int else {
			throw Abort(.badRequest, reason: "Please provide a post ID")
		}
		
		guard let like = try Like.makeQuery().filter("postId", postId).filter("userId", user.id).first() else {
			throw Abort(.badRequest, reason: "User doesn't like this post anyway.")
		}
		
		try like.delete()
		return try JSON(node :["success": true])
	}
}
