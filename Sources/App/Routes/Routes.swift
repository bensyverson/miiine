import Vapor
import AuthProvider

extension Droplet {
	func setupRoutes() throws {
		try setupUnauthenticatedRoutes()
		try setupPasswordProtectedRoutes()
		try setupTokenProtectedRoutes()
	}

	/// Sets up all routes that can be accessed
	/// without any authentication. This includes
	/// creating a new User.
	private func setupUnauthenticatedRoutes() throws {
		// a simple json example response

		get("hello") { req in
			var json = JSON()
			try json.set("hello", "world")
			return json
		}

		// a simple plaintext example response
		get("plaintext") { req in
			return "Hello, world!"
		}

		// response to requests to /info domain
		// with a description of the request
		get("info") { req in
			return req.description
		}
		
		get("invitation", String.parameter) { request in
			let invitationKey = try request.parameters.next(String.self)
			guard let invitation = try Invitation.makeQuery().filter("key", invitationKey).first() else {
				throw Abort(.badRequest, reason: "Invalid invitation.")
			}
			var json = JSON()
			try json.set(Invitation.Keys.token.rawValue, invitation.token)
			return json
		}

		// create a new user
		//
		// POST /user
		// <json containing new user information>
		post("user") { req in
			// require that the request body be json
			guard let json = req.json else {
				throw Abort(.badRequest, reason: "Improperly formed JSON.")
			}
			let authLevel : Authorization
			let invitation : Invitation?
			
			if try User.count() < 1 {
				print("Setting up our first user!")
				authLevel = .owner
				invitation = nil
			} else {
				let token : String = try json.get("token")
				guard let anInvitation = try Invitation.makeQuery().filter("token", token).first() else {
					throw Abort(.badRequest, reason: "Bad invitation.")
				}
				authLevel = anInvitation.authorization
				invitation = anInvitation
			}
			
			print("Setting from JSON")
			let user = try User(json: json)

			// initialize the name and email from
			// the request json

			print("Checking username")
			// ensure no user with this username already exists
			guard try User.makeQuery().filter("username", user.username).first() == nil else {
				throw Abort(.badRequest, reason: "A user with that username already exists.")
			}

			print("Making sure there's a password")
			// require a plaintext password is supplied
			guard let password = json["password"]?.string else {
				throw Abort(.badRequest, reason: "Bad password format")
			}

			// hash the password and set it on the user
			user.password = try self.hash.make(password.makeBytes()).makeString()

			// set the user's authorization level
			user.authorization = authLevel

			// save and return the new user
			try user.save()
			
			try invitation?.delete()
			return user
		}
	}

	/// Sets up all routes that can be accessed using
	/// username + password authentication.
	/// Since we want to minimize how often the username + password
	/// is sent, we will only use this form of authentication to
	/// log the user in.
	/// After the user is logged in, they will receive a token that
	/// they can use for further authentication.
	private func setupPasswordProtectedRoutes() throws {
		// creates a route group protected by the password middleware.
		// the User type can be passed to this middleware since it
		// conforms to PasswordAuthenticatable
		let password = grouped([
			PasswordAuthenticationMiddleware(User.self)
		])

		// verifies the user has been authenticated using the password
		// middleware, then generates, saves, and returns a new access token.
		//
		// POST /login
		// Authorization: Basic <base64 email:password>
		password.post("login") { req in
			let user = try req.user()
			let token = try Token.generate(for: user)
			try token.save()
			return token
		}
	}

	/// Sets up all routes that can be accessed using
	/// the authentication token received during login.
	/// All of our secure routes will go here.
	private func setupTokenProtectedRoutes() throws {
		// creates a route group protected by the token middleware.
		// the User type can be passed to this middleware since it
		// conforms to TokenAuthenticatable
		
		
		let token = grouped([
			TokenAuthenticationMiddleware(User.self)
		])

		// simply returns a greeting to the user that has been authed
		// using the token middleware.
		//
		// GET /me
		// Authorization: Bearer <token from /login>
		token.get("me") { req in
			let user = try req.user()
			return "Hello, \(user.username)"
		}

		// All "posts" requests must be authenticated:
		let postController = PostController(routeBuilder: token)
		postController.addRoutes()
		
		let commentController = CommentController(routeBuilder: token)
		commentController.addRoutes()
		
		let invitationController = InvitationController(routeBuilder: token)
		invitationController.addRoutes()
	}
}
