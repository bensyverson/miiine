import Vapor
import FluentProvider
import HTTP

final class Invitation: Model, Timestampable {
	let storage = Storage()

	// MARK: Properties and database keys
	/// The content of the Invitation
	var key : String
	var token : String
	var name : String
	var authorization : Authorization
	
	/// The column names for the properties in the database
	enum Keys : String {
		case key, token, name, authorization
	}

	/// Creates a new Invitation
	init(key: String, token: String, name: String, authorization: Authorization) throws {
		self.key = key
		self.token = token
		self.name = name
		self.authorization = authorization
	}

	// MARK: Fluent Serialization

	/// Initializes the Invitation from the
	/// database row
	init(row: Row) throws {
		key = try row.get(Invitation.Keys.key.rawValue)
		token = try row.get(Invitation.Keys.token.rawValue)
		name = try row.get(Invitation.Keys.name.rawValue)
		guard let anAuthorization = Authorization(rawValue: try row.get(User.Keys.authorization.rawValue)) else {
			throw Abort(.badRequest, reason: "Invalid authorization raw value")
		}
		authorization = anAuthorization
	}

	// Serializes the Invitation to the database
	func makeRow() throws -> Row {
		var row = Row()
		try row.set(Invitation.Keys.key.rawValue, key)
		try row.set(Invitation.Keys.token.rawValue, token)
		try row.set(Invitation.Keys.name.rawValue, name)
		try row.set(Invitation.Keys.authorization.rawValue, authorization.rawValue)
		return row
	}
}

// MARK: Fluent Preparation


extension Invitation: Preparation {
	/// Prepares a table/collection in the database
	/// for storing Invitations
	static func prepare(_ database: Database) throws {
		try database.create(self) { builder in
			builder.id()
			builder.string(Invitation.Keys.key.rawValue)
			builder.string(Invitation.Keys.token.rawValue)
			builder.string(Invitation.Keys.name.rawValue)
			builder.string(Invitation.Keys.authorization.rawValue)
		}
	}

	/// Undoes what was done in `prepare`
	static func revert(_ database: Database) throws {
		try database.delete(self)
	}
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//	 - Creating a new Invitation (Invitation /Invitations)
//	 - Fetching a Invitation (GET /Invitations, GET /Invitations/:id)
//
extension Invitation: JSONConvertible {
	convenience init(json: JSON) throws {
		guard let anAuthorization = Authorization(rawValue: try json.get(User.Keys.authorization.rawValue)) else {
			throw Abort(.badRequest, reason: "Invalid authorization raw value")
		}
		try self.init(
			key: json.get(Invitation.Keys.key.rawValue),
			token: json.get(Invitation.Keys.token.rawValue),
			name: json.get(Invitation.Keys.name.rawValue),
			authorization: anAuthorization
		)
	}

	func makeJSON() throws -> JSON {
		var json = JSON()
		try json.set(Invitation.Keys.key.rawValue, key)
		try json.set(Invitation.Keys.name.rawValue, name)
		try json.set(Invitation.Keys.authorization.rawValue, authorization.rawValue)
		return json
	}
}

// MARK: HTTP

// This allows Invitation models to be returned
// directly in route closures
extension Invitation: ResponseRepresentable { }
