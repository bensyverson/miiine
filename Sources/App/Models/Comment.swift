import Vapor
import FluentProvider
import HTTP

final class Comment: Model, Timestampable {
	let storage = Storage()

	// MARK: Properties and database keys
	/// The content of the Comment
	var content: String
	let contentKey: String
	let postId: Identifier
	let userId: Identifier
	
	/// The column names for the properties in the database
	enum Keys : String {
		case id, postId, userId, content, contentKey
	}

	/// Creates a new Comment
	init(post: Post, user: User, contentKey: String, content: String) throws {
		self.content = content
		self.contentKey = contentKey
		postId = try post.assertExists()
		userId = try user.assertExists()
	}

	// MARK: Fluent Serialization

	/// Initializes the Comment from the
	/// database row
	init(row: Row) throws {
		userId = try row.get(User.foreignIdKey)
		postId = try row.get(Post.foreignIdKey)
		content = try row.get(Comment.Keys.content.rawValue)
		contentKey = try row.get(Comment.Keys.contentKey.rawValue)
	}

	// Serializes the Comment to the database
	func makeRow() throws -> Row {
		var row = Row()
		try row.set(Comment.Keys.content.rawValue, content)
		try row.set(Comment.Keys.contentKey.rawValue, contentKey)
		try row.set(User.foreignIdKey, userId)
		try row.set(Post.foreignIdKey, postId)
		return row
	}
}

// MARK: Fluent Preparation

extension Comment {
	/// Fluent relation for accessing the user
	var user: Parent<Comment, User> {
		return parent(id: userId)
	}
	
	var post: Parent<Comment, Post> {
		return parent(id: postId)
	}
}

extension Comment: Preparation {
	/// Prepares a table/collection in the database
	/// for storing Comments
	static func prepare(_ database: Database) throws {
		try database.create(self) { builder in
			builder.id()
			builder.string(Comment.Keys.content.rawValue)
			builder.string(Comment.Keys.contentKey.rawValue)
			builder.foreignId(for: User.self)
			builder.foreignId(for: Post.self)
		}
	}

	/// Undoes what was done in `prepare`
	static func revert(_ database: Database) throws {
		try database.delete(self)
	}
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//	 - Creating a new Comment (Comment /Comments)
//	 - Fetching a Comment (GET /Comments, GET /Comments/:id)
//
extension Comment: JSONConvertible {
	convenience init(json: JSON) throws {
		let userId : Identifier = try json.get(Comment.Keys.userId.rawValue)
		guard let user = try User.find(userId.string) else {
			throw Abort(.badRequest, reason: "Invalid User ID")
		}
		let postId : Identifier = try json.get(Comment.Keys.postId.rawValue)
		guard let post = try Post.find(postId.string) else {
			throw Abort(.badRequest, reason: "Invalid Post ID")
		}
		let content : String = try json.get(Comment.Keys.content.rawValue)
		let contentKey : String = try json.get(Comment.Keys.contentKey.rawValue)
		try self.init(post: post, user: user, contentKey: contentKey, content: content)
	}

	func makeJSON() throws -> JSON {
		var json = JSON()
		try json.set(Comment.Keys.id.rawValue, id)
		try json.set(Comment.Keys.userId.rawValue, userId)
		try json.set(Comment.Keys.content.rawValue, content)
		return json
	}
}

// MARK: HTTP

// This allows Comment models to be returned
// directly in route closures
extension Comment: ResponseRepresentable { }

// MARK: Update

// This allows the Comment model to be updated
// dynamically by the request.
extension Comment: Updateable {
	// Updateable keys are called when `Comment.update(for: req)` is called.
	// Add as many updateable keys as you like here.
	public static var updateableKeys: [UpdateableKey<Comment>] {
		return [
			// If the request contains a String at key "content"
			// the setter callback will be called.
			UpdateableKey(Comment.Keys.content.rawValue, String.self) { Comment, content in
				Comment.content = content
			}
		]
	}
}
