import Vapor
import FluentProvider
import HTTP

final class Like: Model, Timestampable {
	let storage = Storage()

	// MARK: Properties and database keys
	/// The content of the Like
	let postId: Identifier
	let userId: Identifier
	
	/// The column names for the properties in the database
	enum Keys : String {
		case id, postId, userId
	}

	/// Creates a new Like
	init(post: Post, user: User) throws {
		postId = try post.assertExists()
		userId = try user.assertExists()
	}

	// MARK: Fluent Serialization

	/// Initializes the Like from the
	/// database row
	init(row: Row) throws {
		userId = try row.get(User.foreignIdKey)
		postId = try row.get(Post.foreignIdKey)
	}

	// Serializes the Like to the database
	func makeRow() throws -> Row {
		var row = Row()
		try row.set(User.foreignIdKey, userId)
		try row.set(Post.foreignIdKey, postId)
		return row
	}
}

// MARK: Fluent Preparation

extension Like {
	/// Fluent relation for accessing the user
	var user: Parent<Like, User> {
		return parent(id: userId)
	}
	
	var post: Parent<Like, Post> {
		return parent(id: postId)
	}
}

extension Like: Preparation {
	/// Prepares a table/collection in the database
	/// for storing Likes
	static func prepare(_ database: Database) throws {
		try database.create(self) { builder in
			builder.id()
			builder.foreignId(for: User.self)
			builder.foreignId(for: Post.self)
		}
	}

	/// Undoes what was done in `prepare`
	static func revert(_ database: Database) throws {
		try database.delete(self)
	}
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//	 - Creating a new Like (Like /Likes)
//	 - Fetching a Like (GET /Likes, GET /Likes/:id)
//
extension Like: JSONConvertible {
	convenience init(json: JSON) throws {
		let userId : Identifier = try json.get("userId")
		guard let user = try User.find(userId.string) else {
			throw Abort(.badRequest, reason: "Invalid User ID")
		}
		let postId : Identifier = try json.get("postId")
		guard let post = try Post.find(postId.string) else {
			throw Abort(.badRequest, reason: "Invalid Post ID")
		}
		try self.init(post: post, user: user)
	}

	func makeJSON() throws -> JSON {
		var json = JSON()
		try json.set(Like.Keys.userId.rawValue, userId)
		return json
	}
}

// MARK: HTTP

// This allows Like models to be returned
// directly in route closures
extension Like: ResponseRepresentable { }
