import Vapor
import FluentProvider
import AuthProvider
import HTTP

public enum Authorization : String {
	case unauthorized, viewer, contributor, admin, owner
	var intValue : Int {
		switch self {
			case .unauthorized: return -1
			case .viewer: return 0
			case .contributor: return 1
			case .admin: return 2
			case .owner: return 3
		}
	}
}

final class User: Model, Timestampable {
	let storage = Storage()

	// Enums
	enum Keys : String {
		case id, username, password, authorization
	}
	
	/// The name of the user
	// var name: String

	/// The user's email
	var username: String

	/// The user's _hashed_ password
	var password: String?
	
	var authorization : Authorization
	
	/// Creates a new User
	init(username: String, password: String? = nil, authorization: Authorization) {
		// self.name = name
		self.username = username
		self.password = password
		self.authorization = authorization
	}

	// MARK: Row

	/// Initializes the User from the
	/// database row
	init(row: Row) throws {
		// name = try row.get("name")
		username = try row.get(User.Keys.username.rawValue)
		password = try row.get(User.Keys.password.rawValue)
		guard let anAuthorization = Authorization(rawValue: try row.get(User.Keys.authorization.rawValue)) else {
			throw Abort(.badRequest, reason: "Invalid authorization raw value")
		}
		authorization = anAuthorization
	}

	// Serializes the Post to the database
	func makeRow() throws -> Row {
		var row = Row()
		// try row.set("name", name)
		try row.set(User.Keys.username.rawValue, username)
		try row.set(User.Keys.password.rawValue, password)
		try row.set(User.Keys.authorization.rawValue, authorization.rawValue)
		return row
	}
}

// MARK: Preparation

extension User: Preparation {
	/// Prepares a table/collection in the database
	/// for storing Users
	static func prepare(_ database: Database) throws {
		try database.create(self) { builder in
			builder.id()
			builder.string(User.Keys.username.rawValue)
			builder.string(User.Keys.password.rawValue)
			builder.string(User.Keys.authorization.rawValue)
		}
	}

	/// Undoes what was done in `prepare`
	static func revert(_ database: Database) throws {
		try database.delete(self)
	}
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//	 - Creating a new User (POST /users)
//
extension User: JSONConvertible {
	convenience init(json: JSON) throws {
		let aUsername : String = try json.get(User.Keys.username.rawValue)
		let aPassword : String = try json.get(User.Keys.password.rawValue)
		self.init(username: aUsername, password: aPassword, authorization: .unauthorized)
		id = try json.get(User.Keys.id.rawValue)
	}

	func makeJSON() throws -> JSON {
		var json = JSON()
		try json.set(User.Keys.id.rawValue, id)
		try json.set(User.Keys.username.rawValue, username)
		try json.set(User.Keys.authorization.rawValue, authorization.rawValue)
		return json
	}
}

// MARK: Fluent

extension User {
	var posts: Children<User, Post> {
		return children()
	}
}

// MARK: HTTP

// This allows User models to be returned
// directly in route closures
extension User: ResponseRepresentable { }

// MARK: Password

// This allows the User to be authenticated
// with a password. We will use this to initially
// login the user so that we can generate a token.
extension User: PasswordAuthenticatable {
	var hashedPassword: String? {
		return password
	}
	
	public static var usernameKey : String {
		return User.Keys.username.rawValue
	}

	public static var passwordVerifier: PasswordVerifier? {
		get { return _userPasswordVerifier }
		set { _userPasswordVerifier = newValue }
	}
}

// store private variable since storage in extensions
// is not yet allowed in Swift
private var _userPasswordVerifier: PasswordVerifier? = nil

// MARK: Request

extension Request {
	/// Convenience on request for accessing
	/// this user type.
	/// Simply call `let user = try req.user()`.
	func user() throws -> User {
		print(auth)
		return try auth.assertAuthenticated()
	}
}

// MARK: Token

// This allows the User to be authenticated
// with an access token.
extension User: TokenAuthenticatable {
	typealias TokenType = Token
}
