import Vapor
import FluentProvider
import HTTP

public struct BasicLocation {
	let latitude : Double
	let longitude : Double
	let name : String
}

final class Post: Model, Timestampable {
	let storage = Storage()

	// MARK: Properties and database keys

	/// The caption of the post
	var caption : String
		
	let userId : Identifier
	
	let filename : String
	
	let mimeType : String
	
	let location : BasicLocation?
	
	/// The column names for `id` and `caption` in the database
	enum Keys : String {
		case id, caption, userId, comments, likes, filename, mimeType, latitude, longitude, locationName, location
	}

	/// Creates a new Post
	init(caption: String, user: User, filename: String, mimeType: String, location: BasicLocation?) throws {
		self.caption = caption
		self.filename = filename
		self.mimeType = mimeType
		self.location = location
		userId = try user.assertExists()
	}

	// MARK: Fluent Serialization

	/// Initializes the Post from the
	/// database row
	init(row: Row) throws {
		caption = try row.get(Post.Keys.caption.rawValue)
		filename = try row.get(Post.Keys.filename.rawValue)
		mimeType = try row.get(Post.Keys.mimeType.rawValue)
		if let lat : Double = try? row.get(Post.Keys.latitude.rawValue),
			let lon : Double = try? row.get(Post.Keys.longitude.rawValue),
			let locName : String = try? row.get(Post.Keys.locationName.rawValue) {
			location = BasicLocation(latitude: lat, longitude: lon, name: locName)
		} else {
			location = nil
		}
		
		caption = try row.get(Post.Keys.caption.rawValue)
		userId = try row.get(User.foreignIdKey)
	}

	// Serializes the Post to the database
	func makeRow() throws -> Row {
		var row = Row()
		try row.set(Post.Keys.caption.rawValue, caption)
		try row.set(Post.Keys.filename.rawValue, filename)
		try row.set(Post.Keys.mimeType.rawValue, mimeType)
		if let aLoc = location {
			try row.set(Post.Keys.mimeType.rawValue, aLoc.name)
			try row.set(Post.Keys.latitude.rawValue, aLoc.latitude)
			try row.set(Post.Keys.longitude.rawValue, aLoc.longitude)
		}
		try row.set(User.foreignIdKey, userId)
		return row
	}
}

// MARK: Fluent Preparation

extension Post {
	/// Fluent relation for accessing the user
	var user: Parent<Post, User> {
		return parent(id: userId)
	}
	
	var comments: Children<Post, Comment> {
		return children()
	}
	
	var likes: Children<Post, Like> {
		return children()
	}
}

extension Post: Preparation {
	/// Prepares a table/collection in the database
	/// for storing Posts
	static func prepare(_ database: Database) throws {
		try database.create(self) { builder in
			builder.id()
			builder.string(Post.Keys.caption.rawValue)
			builder.string(Post.Keys.filename.rawValue)
			builder.string(Post.Keys.mimeType.rawValue)
			builder.string(Post.Keys.locationName.rawValue, length: nil, optional: true, unique: false, default: nil)
			builder.double(Post.Keys.latitude.rawValue, optional: true, unique: false, default: nil)
			builder.double(Post.Keys.longitude.rawValue, optional: true, unique: false, default: nil)
			builder.foreignId(for: User.self)
		}
	}

	/// Undoes what was done in `prepare`
	static func revert(_ database: Database) throws {
		try database.delete(self)
	}
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//	 - Creating a new Post (POST /posts)
//	 - Fetching a post (GET /posts, GET /posts/:id)
//
extension Post: JSONConvertible {
	convenience init(json: JSON) throws {
		let userId : Identifier = try json.get(Post.Keys.userId.rawValue)
		guard let user = try User.find(userId.string) else {
			throw Abort(.badRequest, reason: "Invalid User ID")
		}
		let caption : String = try json.get(Post.Keys.caption.rawValue)
		let filename : String = try json.get(Post.Keys.filename.rawValue)
		let mimeType : String = try json.get(Post.Keys.mimeType.rawValue)
		
		let location : BasicLocation?
		if let loc : JSON = try json.get(Post.Keys.location.rawValue),
			let lat : Double = try loc.get(Post.Keys.latitude.rawValue),
			let lon : Double = try loc.get(Post.Keys.longitude.rawValue),
			let locName : String = try loc.get(Post.Keys.locationName.rawValue) {
			location = BasicLocation(latitude: lat, longitude: lon, name: locName)
		} else {
			location = nil
		}

		try self.init(
			caption: caption,
			user: user,
			filename: filename,
			mimeType: mimeType,
			location: location
		)
	}

	func makeJSON() throws -> JSON {
		var json = JSON()
		try json.set(Post.Keys.id.rawValue, id)
		try json.set(Post.Keys.caption.rawValue, caption)
		try json.set(Post.Keys.filename.rawValue, filename)
		try json.set(Post.Keys.mimeType.rawValue, mimeType)
		if let aLoc = location {
			var locJson = JSON()
			try locJson.set(Post.Keys.latitude.rawValue, aLoc.latitude)
			try locJson.set(Post.Keys.longitude.rawValue, aLoc.longitude)
			try locJson.set(Post.Keys.locationName.rawValue, aLoc.name)
			try json.set(Post.Keys.location.rawValue, locJson)
		}
		try json.set(Post.Keys.userId.rawValue, userId)
		try json.set(Post.Keys.comments.rawValue, self.comments.all())
		try json.set(Post.Keys.likes.rawValue, self.likes.all())
		return json
	}
}

// MARK: HTTP

// This allows Post models to be returned
// directly in route closures
extension Post: ResponseRepresentable { }

// MARK: Update

// This allows the Post model to be updated
// dynamically by the request.
extension Post: Updateable {
	// Updateable keys are called when `post.update(for: req)` is called.
	// Add as many updateable keys as you like here.
	public static var updateableKeys: [UpdateableKey<Post>] {
		return [
			// If the request contains a String at key "caption"
			// the setter callback will be called.
			UpdateableKey(Post.Keys.caption.rawValue, String.self) { post, caption in
				post.caption = caption
			}
		]
	}
}
