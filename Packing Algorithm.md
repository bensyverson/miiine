## Definitions

Length: Longest
Width: 2nd Longest
Height/Depth: 3rd Longest

## Packing Algorithm
Given a fixed set of Box sizes and Items in an Order,
And starting with the largest item in a list of Items sorted from largest to smallest:

1. Add the item to the smallest box that is larger than it in all dimensions. Place the item.
	- If no box will contain the item, throw an error
2. Take the next item and calculate the remaining volume in the previous box.
	- If the item could not fit, increase the box size.
		- If there is no higher box size, go to 1.
3. Place the item
	5. If it doesn't fit, try many orientations of all the items until a solution is found (see below)
		6. If there is no solution, increase the box size and retry 4.
			7. If there is no higher box size, go to 1.

## Placement algorithm

Given a box in landscape orientation (X is the width and Y is the height):
If the item's length is greater than X, orient the item in portrait orientation (VHS box laying flat)
	Otherwise, orient the item in landscape orientation (VHS box laying flat rotated 90°)
Add the item's oriented width to the right of the previous item



## Reorientation Algorithm
Given a set of items that don't fit in the box,
And starting with the Items sorted from largest to smallest:

1.