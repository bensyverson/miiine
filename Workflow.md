# Cashish workflow

1. Add to cart
	- Add to list of sku_Objects
	- [Optional]: Display Payment button
	- Passively collect shipping details, use it to lock Check out button
2. Click Check out
	- POST /order
		- [Callback]: GET /tax (return 10.25 for IL billing or shipping addresses)
		- For each SKU, get SKU from database. If not found, create it:
			- GET https://api.stripe.com/v1/skus/sku_Object
			- Store payload in SKU object
		- Store or_Object (entire JSON) and create sanitized ID
		- Return shipping options or error with sanitized order ID
3. [Optional]: Select shipping method
	- PATCH /order/sanitizedId
		- Fetch Stripe order by sanitizedId
			- throw if order status != created
		- POST https://api.stripe.com/v1/orders/{ORDER_ID} (update order with new shipping)
			selected_shipping_method = "id"
		- Store or_Object payload
		- Return new JSON payload
4.Click Pay
	- Pop Stripe Checkout form with the amount
		- POST /charge (with tok_Object)
		- Store charge JSON payload
		- Update order object with "paid" status
		- Send email with order status
		- Every 2 hours, get all "paid" orders:
			- POST https://shippo/transaction with
				- See https://goshippo.com/docs/stripe
			- Store Shippo JSON payload
			- Send email with tracking information
			- POST https://api.stripe.com/v1/orders/{ORDER_ID} (mark order as filled)
			- Update order object with "fulfilled" status and tracking number
5. Display success, offer to create account / login
	- On login, pass tok_Object
		- [If new] POST https://api.stripe.com/v1/customers (with tok_Object)
			- Get all past Orders
			- POST https://api.stripe.com/v1/charges/{CHARGE_ID} (update orders with customerId on Stripe)
